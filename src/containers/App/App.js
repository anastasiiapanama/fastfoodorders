import React, {useState} from 'react';
import {nanoid} from "nanoid";
import './App.css';
import Menu from "../../components/Menu/Menu";

const App = () => {
 const [menu, setMenu] = useState([
     {name: 'Hamburger', price: 80, count: 0, id: nanoid()},
     {name: 'Cheeseburger', price: 90, count: 0, id: nanoid()},
     {name: 'Fries', price: 45, count: 0, id: nanoid()},
     {name: 'Coffee', price: 70, count: 0, id: nanoid()},
     {name: 'Tea', price: 50, count: 0, id: nanoid()},
     {name: 'Cola', price: 40, count: 0, id: nanoid()},
 ]);

 const countOrder = id => {
     const index = menu.findIndex(i => i.id === id);
     const menuCopy = [...menu];
     const menuObjCopy = menuCopy[index];
     menuObjCopy.count++;

     setMenu(menuCopy);
 }

 const deleteOrder = id => {
     const index = menu.findIndex(i => i.id === id);
     const menuCopy = [...menu];
     const menuObjCopy = menuCopy[index];
     menuObjCopy.count--;

     setMenu(menuCopy);
 }

 const totalPrice = () => {
     let total = 0;

     for(let i = 0; i < menu.length; i++) {
         const menuObj = menu[i];
         const name = menuObj.name;
         const price = menuObj.price;

         const menuItem = menu.filter(el => el.name === name)[0].count;

         total += price * menuItem;
     }

     return total;
 }

 return (
   <div className="container">
       <div className="order-items">
           <h3 className="order-head">Order Details:</h3>
           <div className="order-list">

           </div>
               <h6>Total price: {totalPrice()} KGS</h6>
       </div>
       <div className="order-items">
           <h3 className="order-head">Add items:</h3>

           <div className="menu-items">
               <Menu
                   menu={menu}
                   countOrder={countOrder}
                   deleteOrder={deleteOrder}
               />
           </div>
       </div>
   </div>
 );
};

export default App;
