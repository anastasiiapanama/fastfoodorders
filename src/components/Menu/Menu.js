import React from 'react';
import Category from "../Category/Category";

const Menu = props => {
    return props.menu.map(menu => (
       <Category
           key={menu.id}
           countOrder={() => props.countOrder(menu.id)}
           name={menu.name}
           price={menu.price}
           count={menu.count}
           deleteOrder={() => props.deleteOrder(menu.id)}
       />
    ));
};

export default Menu;