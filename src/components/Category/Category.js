import React from 'react';

const Category = props => {
    return (
        <div className="category-menu">
            <button onClick={props.countOrder}>Add</button>
            <p>{props.name}</p>
            <p>Price: {props.price} KGS</p>
            <p>x {props.count}</p>
            <button onClick={props.deleteOrder}>Delete</button>
        </div>
    );
};

export default Category;